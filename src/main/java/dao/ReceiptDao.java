/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import model.User;

/**
 *
 * @author A_R_T
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = 1;

        try {
            String sql = "INSERT INTO receipt (customer_id, user_id, total) VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getId());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }

            for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO receipt (receipt_id, product_id, price, amount) VALUES (?,?,?,?);";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getReceipt().getId());
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setDouble(3, r.getPrice());
                stmtDetail.setInt(4, r.getAmount());
                int rowDetail = stmt.executeUpdate();
                ResultSet resultDetail = stmt.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error: to create receipt");
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT r.id as id,\n"
                    + "       created,\n"
                    + "       customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       user_id,\n"
                    + "       u.name as user_name,\n"
                    + "       u.tel as user_tel,\n"
                    + "       total\n"
                    + "  FROM receipt r, customer c, user u\n"
                    + "  WHERE r.customer_id = c.id AND r.user_id = u.id"
                    + " ORDER BY created DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                Date created = result.getDate("created");
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(id, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel)
                );
                list.add(receipt);
            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all receipt!!");
        }
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id
    ) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,\n"
                    + "       created,\n"
                    + "       customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       user_id,\n"
                    + "       u.name as user_name,\n"
                    + "       u.tel as user_tel,\n"
                    + "       total\n"
                    + "  FROM receipt r, customer c, user u\n"
                    + "  WHERE r.id = ? AND r.customer_id = c.id AND r.user_id = u.id;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int rid = result.getInt("id");
                Date created = result.getDate("created");
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(rid, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel)
                );
                return receipt;
            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id" + id + "!!");
        }
        return null;
    }

    @Override
    public int delete(int id
    ) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM receipt WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete receipt id" + id + "!!");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object
    ) {
//        Connection conn = null;
//       Database db = Database.getInstance();
        //       conn = db.getConnection();
        //       int row = 0;

        //      try {
        //           String sql = "UPDATE product SET name = ?, price = ? WHERE id = ?";
        //           PreparedStatement stmt = conn.prepareStatement(sql);
        //          stmt.setString(1, object.getName());
        //           stmt.setDouble(2, object.getPrice());
        //           stmt.setInt(3, object.getId());
        //           row = stmt.executeUpdate();
//            System.out.println("Affect row " + row);
//
        //       } catch (SQLException ex) {
        //         Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        //     }
        //      db.close();
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "Latte", 35);
        Product p2 = new Product(2, "Espresso", 50);
        User seller = new User(1, "Ten Lee", "0999999999");
        Customer customer = new Customer("WinWin", "0621111111");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        System.out.println(receipt);
        ReceiptDao dao = new ReceiptDao();
        System.out.println("id = " + dao.add(receipt));
        System.out.println("Receipt after add: " + receipt);
        System.out.println("Get all:" + dao.getAll());
    }

}
